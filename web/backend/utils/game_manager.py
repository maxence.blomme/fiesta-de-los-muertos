﻿import asyncio
import re

from asgiref.sync import sync_to_async
from backend.models import Skull, Game, Guess, GameConstraint, CustomCharacter
from backend.utils.game_states import GameStateEnum
from backend.utils.player_manager import delete_players

from backend.utils.redis import Redis


def valid_game_id(game_id):
    return 0 < len(game_id) <= 32 and re.match(r"^[a-zA-Z0-9]+$", game_id)


def game_exists(game_id):
    return len(Game.objects.filter(game_id=game_id)) > 0


def game_started(game_id):
    return Game.objects.filter(game_id=game_id)[0].started


def set_game_started(game_id, value):
    queryset = Game.objects.filter(game_id=game_id)
    queryset.update(started=value)


def delete_game(game_id):
    Skull.objects.filter(game_id=game_id).delete()
    Guess.objects.filter(game_id=game_id).delete()
    GameConstraint.objects.filter(game_id=game_id).delete()
    Game.objects.filter(game_id=game_id).delete()


def delete_custom_theme(game_id):
    CustomCharacter.objects.filter(game_id=game_id).delete()


def clean_game(game_id):
    delete_game(game_id)
    delete_players(game_id)
    delete_custom_theme(game_id)


def get_state(game_id):
    return GameStateEnum(Game.objects.filter(game_id=game_id)[0].state)


def set_state(game_id, state):
    Game.objects.filter(game_id=game_id).update(state=state.value)


def set_constraint_amount(game_id, amount):
    Game.objects.filter(game_id=game_id).update(constraints=amount)


def set_theme(game_id, use_theme, random_themes, theme1, theme2, custom_theme=None):
    if use_theme is not None:
        Game.objects.filter(game_id=game_id).update(use_theme=use_theme)
    if random_themes is not None:
        Game.objects.filter(game_id=game_id).update(random_themes=random_themes)
    if theme1 is not None:
        Game.objects.filter(game_id=game_id).update(theme1=theme1)
    if theme2 is not None:
        Game.objects.filter(game_id=game_id).update(theme2=theme2)
    if Game.objects.filter(game_id=game_id)[0].theme1 == 'custom' and custom_theme is not None:
        CustomCharacter.objects.filter(game_id=game_id).delete()
        for character in custom_theme:
            CustomCharacter.objects.create(game_id=game_id, character=character)


def is_private(game_id):
    return Game.objects.filter(game_id=game_id)[0].private


def join(game_id):
    Game.objects.filter(game_id=game_id).update(joined=True)


def play_word(game_id, state, skull_index, word):
    queryset = Skull.objects.filter(game_id=game_id, index=skull_index)
    if state == GameStateEnum.FST_WORD:
        queryset.update(fst_word=word)
    elif state == GameStateEnum.SEC_WORD:
        queryset.update(sec_word=word)
    elif state == GameStateEnum.TRD_WORD:
        queryset.update(trd_word=word)
    elif state == GameStateEnum.FTH_WORD:
        queryset.update(fth_word=word)


def store_guesses(game_id, player_index, guesses):
    for key in guesses:
        Guess.objects.create(game_id=game_id,
                             player_index=player_index,
                             skull_index=int(key),
                             character_index=guesses[key])


def next_state(game_id):
    state = get_state(game_id)

    if state == GameStateEnum.LOBBY:
        state = GameStateEnum.FST_WORD
    elif state == GameStateEnum.FST_WORD:
        state = GameStateEnum.SEC_WORD
    elif state == GameStateEnum.SEC_WORD:
        state = GameStateEnum.TRD_WORD
    elif state == GameStateEnum.TRD_WORD:
        state = GameStateEnum.FTH_WORD
    elif state == GameStateEnum.FTH_WORD:
        state = GameStateEnum.GUESSES
    elif state == GameStateEnum.GUESSES:
        state = GameStateEnum.FINISHED
    elif state == GameStateEnum.FINISHED:
        state = GameStateEnum.LOBBY

    set_state(game_id, state)

    return state


def set_prop_deleting_game(game_id):
    Redis().set("deleting-" + game_id, 1)


def get_prop_deleting_game(game_id):
    return Redis().get("deleting-" + game_id)


def del_prop_deleting_game(game_id):
    Redis().delete("deleting-" + game_id)


async def start_game_deletion_delay(game_id):
    await asyncio.sleep(10)
    if not get_prop_deleting_game(game_id):
        return
    await asyncio.sleep(600)
    if get_prop_deleting_game(game_id):
        await sync_to_async(clean_game)(game_id)
