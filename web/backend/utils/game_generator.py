﻿import random

from django.db.models import Q

from backend.models import Character, ThemedCharacter, CustomCharacter, Skull, Game, GameConstraint, Constraint
from backend.utils.game_states import GameStateEnum
from backend.utils.game_manager import set_theme
from backend.utils.player_getters import get_players
from backend.utils.player_utils import set_player_index


def generate_game(game_id, private):
    Game.objects.create(game_id=game_id,
                        state=GameStateEnum.LOBBY.value,
                        private=private)


def init_game(game_id):
    game = Game.objects.filter(game_id=game_id)[0]
    players = get_players(game_id)
    characters = select_characters(game_id, game.use_theme, game.random_themes, game.theme1, game.theme2)
    constraints = select_constraints(game.constraints)
    indexes = list(range(len(players)))
    random.shuffle(indexes)

    for i, index in enumerate(indexes):
        set_player_index(game_id, players[i].name, index)
        Skull.objects.create(game_id=game_id,
                             index=index,
                             character=characters[index],
                             played=True)

    for i in range(len(players), 8):
        Skull.objects.create(game_id=game_id,
                             index=i,
                             character=characters[i],
                             played=False)

    for i in range(len(constraints)):
        GameConstraint.objects.create(game_id=game_id,
                                      index=i,
                                      constraint=constraints[i])


def select_characters(game_id, use_theme, random_themes, theme1, theme2):
    if not use_theme:
        characters = Character.objects.all()
    else:
        if random_themes or theme1 != 'custom': 
            if random_themes:
                themes = list(map(lambda e: e['theme'],
                                  list(ThemedCharacter.objects.values('theme').distinct().order_by('?')[:2])))
                set_theme(game_id, None, None, themes[0], themes[1])
            else:
                themes = [theme1, theme2]
            characters = ThemedCharacter.objects.filter(Q(theme=themes[0]) | Q(theme=themes[1])).order_by('?')
        else:
            custom_characters = list(CustomCharacter.objects.filter(game_id=game_id).order_by('?'))
            if theme2 == '':
                sec_characters = []
            elif theme2 == 'default':
                sec_characters = Character.objects.all().order_by('?')
            else:
                sec_characters = ThemedCharacter.objects.filter(theme=theme2).order_by('?')
            characters = []
            for i in range(8):
                if i >= len(custom_characters):
                    characters.append(sec_characters[i])
                elif i >= len(sec_characters):
                    characters.append(custom_characters[i])
                else:
                    characters.append(custom_characters[i] if random.randint(0, 1) else sec_characters[i])
    return list(map(lambda w: w.character, random.sample(list(characters), k=8)))


def select_constraints(amount):
    constraints = Constraint.objects.all()
    return list(map(lambda w: w.constraint, random.sample(list(constraints), k=amount)))
