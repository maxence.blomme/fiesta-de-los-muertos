from django.db.models import Q
import random

from backend.models import Game, Skull, Guess, Character, ThemedCharacter, CustomCharacter
from backend.utils.game_manager import get_state
from backend.utils.game_states import GameStateEnum
from backend.utils.player_utils import get_player_count


def state_finished(game_id):
    state = get_state(game_id)

    if state == GameStateEnum.FST_WORD:
        return all(map(lambda skull: len(skull.fst_word) > 0, Skull.objects.filter(game_id=game_id, played=True)))
    elif state == GameStateEnum.SEC_WORD:
        return all(map(lambda skull: len(skull.sec_word) > 0, Skull.objects.filter(game_id=game_id, played=True)))
    elif state == GameStateEnum.TRD_WORD:
        return all(map(lambda skull: len(skull.trd_word) > 0, Skull.objects.filter(game_id=game_id, played=True)))
    elif state == GameStateEnum.FTH_WORD:
        return all(map(lambda skull: len(skull.fth_word) > 0, Skull.objects.filter(game_id=game_id, played=True)))
    elif state == GameStateEnum.GUESSES:
        for i in range(get_player_count(game_id)):
            if len(Guess.objects.filter(game_id=game_id, player_index=i)) == 0:
                return False
        return True


def random_characters(game_id):
    game = Game.objects.filter(game_id=game_id)[0]
    if not game.use_theme:
        characters = Character.objects.all().order_by('?')
    elif game.theme1 != 'custom':
        characters = ThemedCharacter.objects.filter(Q(theme=game.theme1) | Q(theme=game.theme2)).order_by('?')
    else:
        custom_characters = list(CustomCharacter.objects.filter(game_id=game_id).order_by('?'))
        if game.theme2 == '':
            sec_characters = []
        elif game.theme2 == 'default':
            sec_characters = Character.objects.all().order_by('?')
        else:
            sec_characters = ThemedCharacter.objects.filter(theme=game.theme2).order_by('?')
        characters = []
        for i in range(9):
            if len(custom_characters) <= i < len(sec_characters):
                characters.append(sec_characters[i])
            elif len(sec_characters) <= i < len(custom_characters):
                characters.append(custom_characters[i])
            elif i < len(custom_characters) and i < len(sec_characters):
                characters.append(custom_characters[i] if random.randint(0, 1) else sec_characters[i])

    return list(characters[:9])


def change_skull_character(game_id, skull_index, new_character):
    Skull.objects.filter(game_id=game_id, index=skull_index).update(character=new_character)
