from django.db import models


class Character(models.Model):
    character = models.CharField(max_length=48)


class ThemedCharacter(models.Model):
    character = models.CharField(max_length=48)
    theme = models.CharField(max_length=16)


class CustomCharacter(models.Model):
    character = models.CharField(max_length=48)
    game_id = models.CharField(max_length=32)


class Constraint(models.Model):
    constraint = models.CharField(max_length=48)


class GameConstraint(models.Model):
    game_id = models.CharField(max_length=32)
    index = models.IntegerField()
    constraint = models.CharField(max_length=48)


class Skull(models.Model):
    game_id = models.CharField(max_length=32)
    index = models.IntegerField()
    character = models.CharField(max_length=48)
    played = models.BooleanField(default=False)
    fst_word = models.CharField(max_length=32, default="")
    sec_word = models.CharField(max_length=32, default="")
    trd_word = models.CharField(max_length=32, default="")
    fth_word = models.CharField(max_length=32, default="")


class Guess(models.Model):
    game_id = models.CharField(max_length=32)
    player_index = models.IntegerField()
    skull_index = models.IntegerField()
    character_index = models.IntegerField()


class Player(models.Model):
    game_id = models.CharField(max_length=32)
    index = models.IntegerField(default=-1)
    name = models.CharField(max_length=16)
    connected = models.BooleanField(default=True)


class Game(models.Model):
    game_id = models.CharField(max_length=32)
    state = models.IntegerField()
    private = models.BooleanField(default=False)
    started = models.BooleanField(default=False)
    joined = models.BooleanField(default=False)
    constraints = models.IntegerField(default=0)
    use_theme = models.BooleanField(default=False)
    random_themes = models.BooleanField(default=True)
    theme1 = models.CharField(max_length=16, default='')
    theme2 = models.CharField(max_length=16, default='')
