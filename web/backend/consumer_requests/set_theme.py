﻿from channels.db import database_sync_to_async

import json

from backend.consumer_requests.consumer_request import ConsumerRequest
from backend.consumer_requests.request_types import RequestTypeEnum
from backend.utils.game_manager import set_theme


class SetThemeConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        use_theme = text_data_json['useTheme'] if 'useTheme' in text_data_json else None
        random_themes = text_data_json['randomThemes'] if 'randomThemes' in text_data_json else None
        theme1 = text_data_json['theme1'] if 'theme1' in text_data_json else None
        theme2 = text_data_json['theme2'] if 'theme2' in text_data_json else None
        custom_theme = text_data_json['customTheme'] if 'customTheme' in text_data_json else None
        await database_sync_to_async(set_theme)(consumer.game_id, use_theme, random_themes, theme1, theme2, custom_theme)

        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.SET_THEME.value,
                'use_theme': use_theme,
                'random_themes': random_themes,
                'theme1': theme1,
                'theme2': theme2,
                'customTheme': custom_theme
            }
        )

    @staticmethod
    async def respond(consumer, event):
        # Send message to WebSocket
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.SET_THEME.value,
            'useTheme': event['use_theme'],
            'randomThemes': event['random_themes'],
            'theme1': event['theme1'],
            'theme2': event['theme2'],
            'customTheme': event['customTheme']
        }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return ('useTheme' in text_data_json and isinstance(text_data_json['useTheme'], bool)) or \
               ('randomThemes' in text_data_json and isinstance(text_data_json['randomThemes'], bool)) or \
               ('theme1' in text_data_json and isinstance(text_data_json['theme1'], str)) or \
               ('theme2' in text_data_json and isinstance(text_data_json['theme1'], str)) or \
               ('customTheme' in text_data_json and isinstance(text_data_json['customTheme'], list))
