﻿import json

from channels.db import database_sync_to_async

from backend.consumer_requests.consumer_request import ConsumerRequest
from backend.consumer_requests.request_types import RequestTypeEnum
from backend.utils.game_getters import get_game
from backend.utils.game_manager import del_prop_deleting_game


class RemovePlayerConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        pass

    @staticmethod
    async def respond(consumer, event):
        del_prop_deleting_game(consumer.game_id)

        name = event['name']
        game = await database_sync_to_async(get_game)(consumer.game_id, True)

        # Send message to WebSocket
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.REMOVE_PLAYER.value,
            'name': name
        }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return True
