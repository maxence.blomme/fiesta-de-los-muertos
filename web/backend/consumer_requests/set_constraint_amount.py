﻿from channels.db import database_sync_to_async

import json

from backend.consumer_requests.consumer_request import ConsumerRequest
from backend.consumer_requests.request_types import RequestTypeEnum
from backend.utils.game_manager import set_constraint_amount


class SetConstraintAmountConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        amount = text_data_json['amount']
        await database_sync_to_async(set_constraint_amount)(consumer.game_id, amount)

        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.SET_CONSTRAINT_AMOUNT.value,
                'amount': amount,
            }
        )

    @staticmethod
    async def respond(consumer, event):
        # Send message to WebSocket
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.SET_CONSTRAINT_AMOUNT.value,
            'amount': event['amount'],
        }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return 'amount' in text_data_json and 0 <= text_data_json['amount'] <= 4
