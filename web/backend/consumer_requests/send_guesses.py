﻿import json

from channels.db import database_sync_to_async

from backend.consumer_requests.consumer_request import ConsumerRequest
from backend.consumer_requests.request_types import RequestTypeEnum
from backend.utils.game_manager import store_guesses, next_state
from backend.utils.game_utils import state_finished
from backend.utils.game_getters import get_game, serialize_game
from backend.utils.player_utils import get_player_count, has_guesses


class SendGuessesConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        game_id = consumer.game_id
        player_name = consumer.player_name
        player_index = consumer.index

        if await database_sync_to_async(has_guesses)(game_id, player_index):
            return

        await database_sync_to_async(store_guesses)(game_id, player_index, text_data_json['guesses'])

        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.SEND_GUESSES.value,
            'error': 0,
            'ack': True
        }))

        new_state = await database_sync_to_async(state_finished)(game_id)
        if new_state:
            await database_sync_to_async(next_state)(game_id)

        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.SEND_GUESSES.value,
                'player': player_name,
                'new_state': new_state,
            }
        )

    @staticmethod
    async def respond(consumer, event):
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.SEND_GUESSES.value,
            'error': 0,
            'game': await database_sync_to_async(get_game)(consumer.game_id, True),
            'player': event['player'],
        }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        if 'guesses' not in text_data_json:
            return False

        player_count = await database_sync_to_async(get_player_count)(consumer.game_id)
        guesses = text_data_json['guesses']
        for i in range(player_count):
            if str(i) not in guesses or guesses[str(i)] < 0 or guesses[str(i)] > 7:
                return False

        return True
