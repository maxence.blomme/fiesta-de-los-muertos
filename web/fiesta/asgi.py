"""
ASGI config for fiesta project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/asgi/
"""

import os
import django

import channels.layers
from channels.routing import get_default_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'fiesta.settings')

django.setup()

application = get_default_application()

channel_layer = channels.layers.get_channel_layer()
