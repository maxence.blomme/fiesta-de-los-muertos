#!/usr/bin/python

import os, sys, re, sqlite3
from enum import Enum

try:
    import MySQLdb
except:
    pass
try:
    import psycopg2
except:
    pass


class DbTypeEnum(Enum):
    MYSQL = "0"
    SQLITE = "1"
    POSTGRES = "2"


try:
    sys.argv[1]
except IndexError:
    print("Error - missing input directory name! Usage ./populate_database.py indir_characters infile_constraints")
    sys.exit(1)
else:
    characters_dir = sys.argv[1]

try:
    sys.argv[2]
except IndexError:
    print("Error - missing second input file name! Usage ./populate_database.py infile_characters infile_constraints")
    sys.exit(1)
else:
    constraints_file = sys.argv[2]

db_type = DbTypeEnum.MYSQL
try:
    sys.argv[3]
except IndexError:
    pass
else:
    db_type = DbTypeEnum(sys.argv[3])

# Check on whether file exists and is accessible
if not os.path.isdir(characters_dir):
    print("Error - input directory", characters_dir, "is not accessible!")
    sys.exit(1)

if not os.path.isfile(constraints_file):
    print("Error - input file", constraints_file, "is not accessible!")
    sys.exit(1)
else:
    f2 = open(constraints_file, "r", encoding='utf8')

if db_type == DbTypeEnum.SQLITE:
    conn = sqlite3.connect('./db.sqlite3')
elif db_type == DbTypeEnum.MYSQL:
    conn = MySQLdb.connect(read_default_file=os.environ['MYSQL_CNF'])
elif db_type == DbTypeEnum.POSTGRES:
    conn = psycopg2.connect("host='" + os.environ['POSTGRES_HOST'] + "' " +
                            "port='" + os.environ['POSTGRES_PORT'] + "' " +
                            "dbname='" + os.environ['POSTGRES_DB'] + "' " +
                            "user='" + os.environ['POSTGRES_USER'] + "' " +
                            "password='" + os.environ['POSTGRES_PASSWORD'] + "'");

cursor = conn.cursor()

cursor.execute('DELETE FROM backend_character')
cursor.execute('DELETE FROM backend_themedcharacter')
cursor.execute('DELETE FROM backend_customcharacter')
cursor.execute('DELETE FROM backend_constraint')

count_characters = 0
for filename in os.listdir(characters_dir):
    f = open(os.path.join(characters_dir, filename), "r", encoding='utf8')
    for line in f:
        for character in re.findall(r"[\w )('-]+", line):
            if db_type == DbTypeEnum.SQLITE:
                if filename == 'default':
                    cursor.execute('INSERT INTO backend_character (`character`) VALUES (?)', (character,))
                else:
                    cursor.execute('INSERT INTO backend_themedcharacter (`character`,`theme`) VALUES (?,?)', (character, filename,))
            elif db_type == DbTypeEnum.MYSQL:
                if filename == 'default':
                    cursor.execute('INSERT INTO backend_character (`character`) VALUES (%s)', (character,))
                else:
                    cursor.execute('INSERT INTO backend_themedcharacter (`character`,`theme`) VALUES (%s,%s)', (character, filename,))
            elif db_type == DbTypeEnum.POSTGRES:
                if filename == 'default':
                    cursor.execute('INSERT INTO backend_character ("character") VALUES (%s)', (character,))
                else:
                    cursor.execute('INSERT INTO backend_themedcharacter ("character","theme") VALUES (%s,%s)', (character, filename,))
            count_characters += 1
    f.close()

count_constraints = 0
for line in f2:
    for constraint in re.findall(r"[\w )('-.]+", line):
        if db_type == DbTypeEnum.SQLITE:
            cursor.execute('INSERT INTO backend_constraint (`constraint`) VALUES (?)', (constraint,))
        elif db_type == DbTypeEnum.MYSQL:
            cursor.execute('INSERT INTO backend_constraint (`constraint`) VALUES (%s)', (constraint,))
        elif db_type == DbTypeEnum.POSTGRES:
            cursor.execute('INSERT INTO backend_constraint ("constraint") VALUES (%s)', (constraint,))
        count_constraints += 1

conn.commit()

print(count_characters, "characters added")
print(count_constraints, "constraints added")
f2.close()
