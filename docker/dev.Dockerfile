FROM node:13-alpine AS npm

WORKDIR /react

ADD web/frontend/react/package.json /react/package.json

RUN npm install

ADD web/frontend/react/ /react

RUN npm run build


FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE=fiesta.settings_dev

RUN mkdir /fiesta

WORKDIR /fiesta

RUN apk add --update --no-cache --virtual .tmp \
        gcc           \
        musl-dev      \
        alpine-sdk    \
        libffi-dev    \
        libressl-dev  \
        openssl-dev
RUN pip install --upgrade pip  --no-cache-dir

# More recent cryptography versions don't build in this environment
ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1
RUN pip install cryptography==3.4.7 --no-cache-dir

ADD web/requirements.txt /fiesta/requirements.txt
RUN pip install -r requirements.txt  --no-cache-dir

RUN apk del .tmp

ADD web/ /fiesta/
RUN rm -rf /fiesta/frontend/react

COPY --from=npm /react/build/ /fiesta/frontend/react/build/

ENV REDIS_HOST=redis
ENV REDIS_PORT=6379

CMD python manage.py migrate && \
    python populate_database.py characters constraints 1 && \
    python manage.py runserver 0.0.0.0:65000
