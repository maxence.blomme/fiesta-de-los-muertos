FROM node:12-alpine AS npm

WORKDIR /react

ADD web/frontend/react/package.json /react/package.json

RUN npm install

ADD web/frontend/react/ /react

RUN npm run build


FROM python:3.7-alpine

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# create directory for the app user
RUN mkdir -p /home/app

# create the app user
RUN addgroup -S app && adduser -S app -G app

# create the appropriate directories
ENV APP_HOME=/home/app/web
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

# install dependencies
RUN apk add --update --no-cache --virtual .tmp \
        gcc           \
        musl-dev      \
        alpine-sdk    \
        libffi-dev    \
        libressl-dev  \
        openssl-dev
RUN pip install --upgrade pip --no-cache-dir

# More recent cryptography versions don't build in this environment
ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1
RUN pip install cryptography==3.4.7 --no-cache-dir

ADD web/requirements.txt $APP_HOME/requirements.txt
RUN pip install -r requirements.txt --no-cache-dir

RUN apk add --no-cache postgresql-dev
RUN pip install psycopg2 --no-cache-dir

RUN apk del .tmp

# copy project & remove react project
ADD --chown=app:app web/ $APP_HOME
RUN rm -rf $APP_HOME/frontend/react

COPY --chown=app:app --from=npm /react/build/ $APP_HOME/frontend/react/build/

ENV POSTGRES_HOST=postgres
ENV POSTGRES_PORT=""
ENV POSTGRES_DB=fiesta
ENV POSTGRES_USER=user
ENV POSTGRES_PASSWORD=secret

ENV FIESTA_HOST=localhost
ENV REDIS_HOST=redis
ENV REDIS_PORT=6379

ENV DJANGO_SETTINGS_MODULE=fiesta.settings_prod_postgres

# chown all the files to the app user
RUN chown app:app $APP_HOME

# change to the app user
USER app

RUN python manage.py collectstatic

CMD python manage.py migrate && \
    python populate_database.py characters constraints 2 && \
    python manage.py runserver 0.0.0.0:65000
